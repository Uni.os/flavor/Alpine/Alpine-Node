#
prime='
https://wiki.alpinelinux.org/wiki/Xen_Dom0
https://wiki.alpinelinux.org/wiki/Xen_Dom0_on_USB_or_SD
https://wiki.alpinelinux.org/wiki/Xen_PCI_Passthrough
rel. bootloader.sy
'

# pre
sed -i 's|#http://dl-cdn.alpinelinux.org/alpine/v3.11/community|http://dl-cdn.alpinelinux.org/alpine/v3.11/community|' /etc/apk/repositories
apk update

# Install
apk add xen xen-hypervisor seabios ovmf

echo "xen-netback" >> /etc/modules
echo "xen-blkback" >> /etc/modules
echo "tun" >> /etc/modules

rc-update add xenconsoled
rc-update add xendomains
rc-update add xenqemu
rc-update add xenstored

