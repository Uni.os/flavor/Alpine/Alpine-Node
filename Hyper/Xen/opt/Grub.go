# prime. wiki.alpinelinux.org/wiki/Bootloaders#GRUB

pre(){
apk del syslinux
apk add grub grub-bios
apk add grub-efi
}

Works(){
echo '''GRUB_DEFAULT="Alpine GNU/Linux, with Xen 4 and Linux lts"
''' >> /etc/default/grub

grub-mkconfig -o /boot/grub/grub.cfg
}
